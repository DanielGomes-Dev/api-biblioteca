class LoansController < ApplicationController
  before_action :set_loan, only: [:show, :update, :destroy]

  # GET /loans
  def index
    if !admin_or_clerks
      render json: {err: "you do not have permission"}, status: 401
      return
    end
    @loans = Loan.all

    render json: @loans
  end

  # GET /loans/1
  def show
    if !admin_or_clerks
      render json: {err: "you do not have permission"}, status: 401
      return
    end
    render json: @loan
  end

  # POST /loans
  def create
    if !admin_or_clerks
      render json: {err: "you do not have permission"}, status: 401
      return
    end

    @loan = Loan.new(loan_params)

    if @loan.save
      render json: @loan, status: :created, location: @loan
    else
      render json: @loan.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /loans/1
  def update
    if !admin_or_clerks
      render json: {err: "you do not have permission"}, status: 401
      return
    end
    if @loan.update(loan_params)
      render json: @loan
    else
      render json: @loan.errors, status: :unprocessable_entity
    end
  end

  # DELETE /loans/1
  def destroy
    if !admin_or_clerks
      render json: {err: "you do not have permission"}, status: 401
      return
    end
    @loan.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_loan
      @loan = Loan.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def loan_params
      params.require(:loan).permit(:expire_date, :delay_days, :fine, :book_id, :user_id, :status)
    end
end
