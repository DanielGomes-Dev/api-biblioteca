class AuthenticationController < ApplicationController

  def login
    user = User.find_by(email:user_params[:email]);
    user = user&.authenticate(user_params[:password]);


    if user
      token = JsonWebToken.encode(user_id: user.id);
      render json: {token: token}
    
    else
      render json: {err:"Not Authenticate"}, status: 401
    
    end

  end

  def user_params
    params.require(:user).permit(:email, :password)
  end

end
