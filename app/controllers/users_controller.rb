class UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy]
  # load_and_authorize_resource

  # GET /users
  def index

      ##Apenas Admins e Funcionarios
    if !admin_or_clerks
      render json: {err: "you do not have permission"}, status: 401
      return
    end
    
    @users = User.all

    render json: @users
  end

  # GET /users/1
  def show
    if !admin_or_clerks
      render json: {err: "you do not have permission"}, status: 401
      return
    end

    unless @user
      return render json: {err: "User not Found"}
    end

    render json: @user
    
  end

  # POST /users
  def create

    if !admin_or_clerks
      render json: {err: "you do not have permission"}, status: 401
      return
    end
    
    #Verifica se a role existe || se o usuario atual tem cargo maior ou igual que o usuario que esta sendo criado  || se o cargo existe
    if !user_params[:role] || current_user.role >= user_params[:role] || user_params[:role] > 2 
    
      render json: {err: 'you do not have permission'}, status: 401
      return
    end
    
    @user = User.new(user_params)

    if @user.save
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /users/1
  def update
 
    if !admin_or_clerks
      render json: {err: "you do not have permission"}, status: 401
      return
    end
 
    if current_user.role >= @user.role #Impede que Clientes possam editar usuarios e que funcionarios possam editar outros funcionarios ou admins (Admins Possam editar funcionarios ou Clientes) 

      render json: {err: 'you do not have permission'}, status: 401
      return
    end 

    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1

  
  def destroy

    if !admin_or_clerks
      render json: {err: "you do not have permission"}, status: 401
      return
    end
 
    if current_user.role <= @user.role 
        render json: {err: "you do not have permission"}, status: 401
        return      
      return
    end  


    @user.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation, :name, :cpf, :telephone, :role)
    end
end
