class User < ApplicationRecord
    has_secure_password
    has_one :loan
    # enum role:{
    #     "Admin":0,
    #     "Clerk":1,
    #     "Client":2
    # }

    #Campos Obrigatorios
    validates :email, :password, :password_confirmation, :name, :role, presence: true
    
    #Email
    validates :email, uniqueness: { message: "this email already registrate", case_sensitive: false}


    #Password
    validates :password, length: { in: 6..20 }

    #Name


    #CPF
    validates :cpf, uniqueness: { message: "this cpf already registrate"}


    #Telephone
    validates :telephone, uniqueness: { message: "this cpf already registrate"}

    #Role
    validates :role, numericality: { only_integer: true }


end
