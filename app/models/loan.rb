class Loan < ApplicationRecord
  belongs_to :book
  belongs_to :user


  
   #Date :expire_date
   validates :expire_date, presence: true


   #integer :delay_days


   #boolean :fine
   validates :status, numericality: { only_integer: true }

   #integer :book
   validates :book, presence: true
   validates :book, uniqueness: { message: "this book already used"}

   #integer :user
   validates :user, presence: true
   validates :user, uniqueness: { message: "this user already have a book"}

   #integer :status
   validates :status, numericality: { only_integer: true }

end
