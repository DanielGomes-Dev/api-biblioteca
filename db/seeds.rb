# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(email:"admin@hotmail.com",password:"123456",name:"admin",cpf:"111.111.111-10",telephone:"+55 021 98888-8880",role:0);

User.create(email:"funcionario@hotmail.com",password:"123456",name:"funcionario",cpf:"111.111.111-11",telephone:"+55 021 98888-8881",role:1)

user = User.create(email:"cliente@hotmail.com",password:"123456",name:"cliente",cpf:"111.111.111-12",telephone:"+55 021 98888-8882",role:2)

# autor = Author.create(name:"AutorLegal")

# livro = Book.create(kind: "kind",issue: "issue",theme: "theme",edition: "1",url_image: "http://testt",name: "Livro",author_id: autor);

# puts livro.kind

# emprestimo = Loan.create(expire_date:"12/12/12", delay_days: 3,fine: false, book_id: livro, user_id: user, status: 0);

# puts emprestimo.fine
