Rails.application.routes.draw do
  post '/login', to: 'authentication#login'
  resources :loans
  resources :books
  resources :authors
  resources :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
